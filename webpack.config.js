const path = require("path");
const fs = require("fs");
const webpack = require("webpack");

let nodeModules = {};

fs.readdirSync("./node_modules")
.filter(x => {
    return [".bin"].indexOf(x) === -1;
})
.forEach(mod => {
    nodeModules[mod] = "commonjs " + mod;
});

module.exports = {
    entry: "./dist/main.js",
    target: "node",
    output: {
        filename: "./bundle_nodejsback.js"
    },
    externals: nodeModules,
    mode: "development"
};