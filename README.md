**Node.js TypeORM TS Back-end web application**

## Providing

1. Node.js/Express.js server app
2. Typescript language
3. Bundle creation with Webpack
4. Dependency and Singleton management
5. Service Oriented Architecture
6. TypeORM - ES6 friendly - library for connecting to databases
