import { app, port } from "./config/server";
import chalk from "chalk";
import { buildModules } from "./config/module-manager";

// Builds modules
buildModules();

// Open server to run on the specified port
app.listen(port, err => {
    if(err) {
        console.log(
            chalk.bold.red(err)
        );
    }
    else {
        console.log(
            chalk.bold.green(`TypeBack: Express server listening on port`), 
            chalk.bold.yellow(`${port}`)
        );
    }
});