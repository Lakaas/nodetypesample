import { Account } from "../model/account";
import AccountProvider from "../provider/account-provider";

export default class AccountService {

    constructor(private accountProvider: AccountProvider) {

    }

    async getAll(): Promise<Array<Account>> {
        try {
            return await this.accountProvider.getAll();

        }
        catch(err) {

        }
    }

    async getById(id: number): Promise<Account> {
        try {
            return await this.accountProvider.getById(id);
        }
        catch(err) {

        }
    }

}