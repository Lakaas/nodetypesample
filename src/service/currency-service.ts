import { Currency } from "../model/currency";
import CurrencyProvider from "../provider/currency-provider";

export default class CurrencyService {

    constructor(private currencyProvider: CurrencyProvider) {

    }

    async getAll(): Promise<Array<Currency>> {
        try {
            return await this.currencyProvider.getAll();

        }
        catch(err) {

        }
    }

    async getById(id: number): Promise<Currency> {
        try {
            return await this.currencyProvider.getById(id);
        }
        catch(err) {

        }
    }

}