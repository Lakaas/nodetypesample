import { Customer } from "../model/customer";
import CustomerProvider from "../provider/customer-provider";

export default class CustomerService {

    constructor(private customerProvider: CustomerProvider) {

    }

    async getAll(): Promise<Array<Customer>> {
        try {
            return await this.customerProvider.getAll();

        }
        catch(err) {

        }
    }

    async getById(id: number): Promise<Customer> {
        try {
            return await this.customerProvider.getById(id);
        }
        catch(err) {

        }
    }

}