import { Currency } from "../model/currency";
import { getConnection } from "./../config/db-config";
import { Connection } from "typeorm";

export default class CurrencyProvider {

    private connection: Connection;

    constructor() {
        getConnection()
        .then(connection => {
            this.connection = connection;
        });
    }

    async getAll(): Promise<Array<Currency>> {
        try {
            return this.connection
            .getRepository(Currency)
            .createQueryBuilder("c")
            .getMany();
        }
        catch(err) {

        }
    }

    async getById(id: number): Promise<Currency> {
        try {
            return this.connection
            .getRepository(Currency)
            .createQueryBuilder("c")
            .where("c.id = :id", { id: id })
            .getOne();
        }
        catch(err) {
            
        }
    }

}