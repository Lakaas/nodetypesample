import { Account } from "../model/account";
import { getConnection } from "./../config/db-config";
import { Connection } from "typeorm";

export default class AccountProvider {

    private connection: Connection;

    constructor() {
        getConnection()
        .then(connection => {
            this.connection = connection;
        });
    }

    async getAll(): Promise<Array<Account>> {
        try {
            return this.connection
            .getRepository(Account)
            .createQueryBuilder("a")
            .leftJoinAndSelect("a.customer", "c")
            .leftJoinAndSelect("a.currency", "cr")
            .getMany();
        }
        catch(err) {

        }
    }

    async getById(id: number): Promise<Account> {
        try {
            return this.connection
            .getRepository(Account)
            .createQueryBuilder("a")
            .where("a.id = :id", { id: id })
            .getOne();
        }
        catch(err) {
            
        }
    }

}