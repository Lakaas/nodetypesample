import { Customer } from "../model/customer";
import { getConnection } from "./../config/db-config";
import { Connection } from "typeorm";

export default class CustomerProvider {

    private connection: Connection;

    constructor() {
        getConnection()
        .then(connection => {
            this.connection = connection;
        });
    }

    async getAll(): Promise<Array<Customer>> {
        try {
            return this.connection
            .getRepository(Customer)
            .createQueryBuilder("c")
            .getMany();
        }
        catch(err) {

        }
    }

    async getById(id: number): Promise<Customer> {
        try {
            return this.connection
            .getRepository(Customer)
            .createQueryBuilder("c")
            .where("c.id = :id", { id: id })
            .getOne();
        }
        catch(err) {
            
        }
    }

}