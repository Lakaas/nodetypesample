import {Entity, PrimaryGeneratedColumn, Column, ManyToOne} from "typeorm";
import { Invoice } from "./invoice";
import { Product } from "./product";

@Entity({
    name: "invoice_detail"
})
export class InvoiceDetail {

    @PrimaryGeneratedColumn({
        name: "id",
        type: "int"
    })
    id: number;

    @ManyToOne(type => Invoice)
    @Column({
        name: "invoice_id",
        type: "int"
    })
    invoice: Invoice;

    @ManyToOne(type => Product)
    @Column({
        name: "product_id",
        type: "int"
    })
    product: Product;

}