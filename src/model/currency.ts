import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity({
    name: "currency"
})
export class Currency {

    @PrimaryGeneratedColumn({
        name: "id",
        type: "int"
    })
    id: number;

    @Column({
        name: "iso",
        type: "varchar",
        length: 3,
        nullable: false,
    })
    iso: string;

    @Column({
        name: "name",
        type: "varchar",
        length: 30
    })
    name: string;

    @Column({
        name: "convert_factor",
        type: "decimal"
    })
    convertFactor: number;

}