import {Entity, PrimaryGeneratedColumn, Column, ManyToOne} from "typeorm";
import { Customer } from "./customer";
import { Account } from "./account";

@Entity({
    name: "invoice"
})
export class Invoice {

    @PrimaryGeneratedColumn({
        name: "id",
        type: "int"
    })
    id: number;

    @Column({
        name: "invoice_date",
        type: "timestamp"
    })
    invoiceDate: Date;

    @Column({
        name: "amount",
        type: "decimal"
    })
    amount: number;

    @Column({
        name: "is_paid",
        type: "bit"
    })
    isPaid: boolean;

    @ManyToOne(type => Customer)
    @Column({
        name: "customer_id",
        type: "int"
    })
    customer: Customer;

    @ManyToOne(type => Account)
    @Column({
        name: "account_id",
        type: "int"
    })
    account: Account;

}