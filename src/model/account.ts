import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from "typeorm";
import { Customer } from "./customer";
import { Currency } from "./currency";

@Entity({
    name: "account"
})
export class Account {

    @PrimaryGeneratedColumn({
        name: "id",
        type: "int",
    })
    id: number;

    @Column({
        name: "name",
        type: "varchar",
        length: 30
    })
    name: string;

    @Column({
        name: "amount",
        type: "decimal"
    })
    amount: number;

    @ManyToOne(type => Customer)
    @JoinColumn({
        name: "customer_id"
    })
    customer: Customer;

    @ManyToOne(type => Currency)
    @JoinColumn({ 
        name: "currency_id"
    })
    currency: Currency;

}