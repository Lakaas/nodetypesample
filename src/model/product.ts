import {Entity, PrimaryGeneratedColumn, Column, ManyToOne} from "typeorm";

@Entity({
    name: "product"
})
export class Product {

    @PrimaryGeneratedColumn({
        name: "id",
        type: "int"
    })
    id: number;

    @Column({
        name: "name",
        type: "varchar",
        length: 30
    })
    name: string;

    @Column({
        name: "description",
        type: "varchar",
        length: 300
    })
    description: string;

    @Column({
        name: "unit_price",
        type: "decimal"
    })
    unitPrice: number;

}