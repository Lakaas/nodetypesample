import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity({
    name: "customer"
})
export class Customer {

    @PrimaryGeneratedColumn({
        name: "id",
        type: "int"
    })
    id: number;

    @Column({
        name: "last_name",
        type: "varchar",
        length: 30
    })
    lastName: string;

    @Column({
        name: "first_name",
        type: "varchar",
        length: 30
    })
    firstName: string;

}