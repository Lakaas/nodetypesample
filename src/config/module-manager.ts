import * as bodyParser from "body-parser";
import * as cors from "cors";
import { app, router } from "../config/server";
import CustomerApi from "../api/customer-api";
import CustomerService from "../service/customer-service";
import CustomerProvider from "../provider/customer-provider";
import DefaultApi from "../api/default-api";
import CurrencyProvider from "../provider/currency-provider";
import CurrencyService from "../service/currency-service";
import CurrencyApi from "../api/currency-api";
import AccountProvider from "../provider/account-provider";
import AccountService from "../service/account-service";
import AccountApi from "../api/account-api";


// Builds middlewares that handles api requests
let buildRequestHandler = () => {

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(cors());

}

// Builds application modules
let buildAppModules = () => {
    
    let customerProvider = new CustomerProvider();
    let currencyProvider = new CurrencyProvider();
    let accountProvider = new AccountProvider();
    
    let customerService = new CustomerService(customerProvider);
    let currencyService = new CurrencyService(currencyProvider);
    let accountService = new AccountService(accountProvider);
    
    let customerApi = new CustomerApi(customerService);
    let currencyApi = new CurrencyApi(currencyService);
    let accountApi = new AccountApi(accountService);
    let defaultApi = new DefaultApi();
    
}

// Builds all modules
export let buildModules = () => {
    buildRequestHandler();
    buildAppModules();
}

