import * as path from "path";
import { createConnection, Connection } from "typeorm";

export const getConnection = async () => {
    return await createConnection({
        type: "postgres",
        host: "localhost",
        port: 5432,
        username: "postgres",
        password: "root",
        database: "factory",
        entities: [
            path.join(__dirname, "..", "model", "**{.ts,.js}")
        ],
    });
}