import { router } from "../config/server";
import CustomerService from "../service/customer-service";

const apiRoot = "/api/Customer";

export default class CustomerApi {

    constructor(private customerService: CustomerService) {
        this.define();
    }

    private define() {

        router.get(apiRoot + "/GetAll", async (req, res) => {
            this.customerService.getAll()
            .then(data => {
                res.json(data);
            })
        });

        router.get(apiRoot + "/GetById", async (req, res) => {
            let id: number = req.query.id ? +req.query.id : 0;
            this.customerService.getById(id)
            .then(data => {
                res.json(data);
            });
        })

    }

}