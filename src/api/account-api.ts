import { router } from "../config/server";
import AccountService from "../service/account-service";

const apiRoot = "/api/Account";

export default class AccountApi {

    constructor(private accountService: AccountService) {
        this.define();
    }

    private define() {

        router.get(apiRoot + "/GetAll", async (req, res) => {
            this.accountService.getAll()
            .then(data => {
                res.json(data);
            })
        });

        router.get(apiRoot + "/GetById", async (req, res) => {
            let id: number = req.query.id ? +req.query.id : 0;
            this.accountService.getById(id)
            .then(data => {
                res.json(data);
            });
        })

    }

}