import { app, router } from "./../config/server";

export default class DefaultApi {

    constructor() {
        this.define();
    }

    private define() {
        app.use("/", router);
    }

}