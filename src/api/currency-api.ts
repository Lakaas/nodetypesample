import { router } from "../config/server";
import CurrencyService from "../service/currency-service";

const apiRoot = "/api/Currency";

export default class CurrencyApi {

    constructor(private currencyService: CurrencyService) {
        this.define();
    }

    private define() {

        router.get(apiRoot + "/GetAll", async (req, res) => {
            this.currencyService.getAll()
            .then(data => {
                res.json(data);
            })
        });

        router.get(apiRoot + "/GetById", async (req, res) => {
            let id: number = req.query.id ? +req.query.id : 0;
            this.currencyService.getById(id)
            .then(data => {
                res.json(data);
            });
        })

    }

}